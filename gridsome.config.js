// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: "Gridsome",
  plugins: [
    {
      use: "gridsome-source-graphql-prismic",
      options: {
        url: process.env.API_URL, // @Todo set ENV
        fieldName: "apiDocuments",
        typeName: "api",
        headers: {
          "Prismic-ref": process.env.PRISMIC_REF // @Todo set ENV
        }
      }
    }
  ]
};
