// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
module.exports = function(api) {
  api.loadSource(async ({ addCollection }) => {});

  api.createPages(async ({ createPage, graphql }) => {
    const { data } = await graphql(`
      {
        apiDocuments {
          allDocuments: _allDocuments {
            edges {
              node {
                ... on api_Home_page {
                  title
                  content
                  img
                }
                ... on api_Content_page {
                  title
                  content
                  img
                }
                meta: _meta {
                  id
                  uid
                  type
                }
              }
            }
          }
        }
      }
    `);

    data.apiDocuments.allDocuments.edges.map(({ node }) => {
      if (node.meta.type === "home_page") {
        createPage({
          path: `/`,
          component: "./src/templates/content.vue",
          context: {
            title: node.title[0].text,
            content: node.content
          }
        });
      } else if (node.meta.type === "content_page") {
        createPage({
          path: `/content`,
          component: "./src/templates/content.vue",
          context: {
            title: node.title[0].text,
            content: node.content
          }
        });
      }
    });
  });
};
